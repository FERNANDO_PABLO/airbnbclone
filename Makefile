build:
	docker-compose down
	docker-compose up --build -d 
test : 
	docker compose down
	docker compose -f docker-compose.test.yml up --build -d
	docker-compose exec -it api pytest --disable-warnings
	docker-compose down
